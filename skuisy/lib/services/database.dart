import 'package:skuisy/config/constants.dart';
import 'package:skuisy/models/user_model.dart';

class DbService {
  static void updateUser(User user){
    usersRef.document(user.id).updateData({
      'name': user.name,
      'profileImageUrl': user.profileImageUrl,
      'bio': user.bio,
    });
  }
}