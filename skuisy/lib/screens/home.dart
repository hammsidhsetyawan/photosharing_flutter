import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
import 'package:skuisy/screens/activity.dart';
import 'package:skuisy/screens/feed.dart';
import 'package:skuisy/screens/photo.dart';
import 'package:skuisy/screens/profile.dart';
import 'package:skuisy/screens/search.dart';

class HomeScreen extends StatefulWidget {

  final String userId;
  HomeScreen({this.userId});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentTab = 0;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    // final String currentUserId = Provider.of<UserData>(context).currentUserId;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          'Skuisy',
          style: TextStyle(
              color: Colors.black, fontFamily: 'Billabong', fontSize: 35.0),
        ),
      ),
      body: PageView(
        controller: _pageController,
        children: <Widget>[
          // FeedScreen(currentUserId: currentUserId),
          FeedScreen(),
          SearchScreen(),
          PhotoScreen(),
          ActivityScreen(),
          // ActivityScreen(currentUserId: currentUserId),
          ProfileScreen(
            // currentUserId: currentUserId,
            userId: widget.userId,
          ),
        ],
        onPageChanged: (int index) {
          setState(() {
            _currentTab = index;
          });
        },
      ),
      bottomNavigationBar: CupertinoTabBar(
        backgroundColor: Colors.white,
        currentIndex: _currentTab,
        onTap: (int index) {
          setState(() {
            _currentTab = index;
          });
          _pageController.animateToPage(
            index,
            duration: Duration(milliseconds: 200),
            curve: Curves.easeIn,
          );
        },
        activeColor: Colors.black,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              size: 32.0,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              size: 32.0,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.add_a_photo,
              size: 32.0,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.notifications,
              size: 32.0,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.account_circle,
              size: 32.0,
            ),
          ),
        ],
      ),
    );
  }
}
