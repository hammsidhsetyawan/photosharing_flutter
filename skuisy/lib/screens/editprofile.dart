import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:skuisy/models/user_model.dart';
import 'package:skuisy/services/database.dart';

class EditProfileScreen extends StatefulWidget {
  final User user;

  EditProfileScreen({this.user});

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  // File _profileImage;
  String _name = '';
  String _bio = '';

  @override
  void initState(){
    super.initState();
    _name = widget.user.name;
    _bio = widget.user.bio;
  }

  // _handleImageFromGallery() async {
  //   File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
  //   if (imageFile != null){
  //     setState(() {
  //       _profileImage = imageFile;
  //     });
  //   }
  // }

  _submit() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      String _profileImageUrl = '';
      User user = User(
        id: widget.user.id,
        name: _name,
        profileImageUrl: _profileImageUrl,
        bio: _bio,
      );
      DbService.updateUser(user);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            'Edit Profile',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: EdgeInsets.all(30.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 60.0,
                        backgroundImage:
                            NetworkImage('assets/images/user_placeholder.jpg'),
                      ),
                      FlatButton(
                          onPressed: () => print('Change image'),
                          child: Text(
                            'Change Profile Image',
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 16.0),
                          )),
                      TextFormField(
                        initialValue: _name,
                        // cursorColor: Colors.white,

                        style: TextStyle(fontSize: 18.0),
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.person,
                              size: 30.0,
                              // color: Colors.white,
                            ),
                            labelText: 'Name'),
                        validator: (input) => input.trim().length < 1
                            ? ' Please enter valid name'
                            : null,
                        onSaved: (input) => _name = input,
                      ),
                      TextFormField(
                        initialValue: _bio,
                        cursorColor: Colors.white,
                        style: TextStyle(fontSize: 18.0,),
                        decoration: InputDecoration(
                            icon: Icon(
                              Icons.book,
                              size: 30.0,
                              // color: Colors.white,
                            ),
                            labelText: 'Bio'),
                        validator: (input) => input.trim().length > 150
                            ? 'Please enter maximal 150 characters'
                            : null,
                        onSaved: (input) => _bio = input,
                      ),
                      Container(
                        margin: EdgeInsets.all(40.0),
                        height: 40.0,
                        width: 250.0,
                        child: FlatButton(
                          onPressed: _submit,
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Text(
                            'Save Profile',
                            style: TextStyle(fontSize: 18.0),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )),
        ));
  }
}
