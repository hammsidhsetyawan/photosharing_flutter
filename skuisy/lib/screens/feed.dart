import 'package:flutter/material.dart';
import 'package:skuisy/services/auth.dart';

class FeedScreen extends StatefulWidget {
  static final String id = 'feed';
  @override
  _FeedScreenState createState() => _FeedScreenState();
}

class _FeedScreenState extends State<FeedScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green,
      body: Center(
        child: FlatButton(
          onPressed: () => AuthService.logout(),
          child: Text('LogOut'),
        ),
      )  
    );
  }
}